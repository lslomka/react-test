import React from 'react';
import FittersContainer from '../../components/fitters/Container';
import { FitterProvider } from '../../contexts';
import { Route, Switch } from 'react-router-dom';
import AddEditFitter from '../../components/fitters/addEdit/AddEdit';
import AddFitter from '../../components/fitters/add/AddFitter';

const FitterPage = () => {
	return (
		<>
			<FitterProvider>
				<Switch>
					<Route component={FittersContainer} exact path='/fitters' />
					<Route component={AddFitter} exact path='/fitters/new' />
					<Route component={AddEditFitter} exact path='/fitters/:id' />
				</Switch>
			</FitterProvider>
		</>
	);
};

export default FitterPage;
