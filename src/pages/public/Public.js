import React from 'react';
import Login from './../../components/public/Login';
import { Grid, Typography, makeStyles } from '@material-ui/core';
import { useRouteMatch } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
	header: {
		margin: theme.spacing(0, 0, 5, 0)
	}
}));

export const Public = props => {
	const classes = useStyles();
	const matchLogin = useRouteMatch('/login');

	return (
		<Grid
			container
			position='relative'
			spacing={0}
			direction='column'
			alignItems='center'
			justify='center'
			style={{ minHeight: '100vh' }}>
			<Grid
				item
				container
				xs={12}
				sm={7}
				md={3}
				direction='column'
				alignItems='stretch'
				justify='center'>
				<Typography className={classes.header} component='h1' variant='h3'>
					ReactMonter
				</Typography>
				{(!matchLogin || matchLogin.path === '/login') && (
					<Login routes={props} />
				)}
				{/* {match ==='/reset'&& <Reset />}  */}
			</Grid>
		</Grid>
	);
};
