import React from 'react';
import { Grid, Button } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';

const NotFound = props => {
	return (
		<Grid
			container
			position='relative'
			spacing={0}
			direction='column'
			alignItems='center'
			justify='center'
			style={{ minHeight: '100vh' }}
		>
			<p>NotFound</p>
			<Button component={RouterLink} to='/'>
				Idź do strony głównej
			</Button>
			<p>lub</p>
			<Button
				variant='text'
				onClick={() => {
					props.history.goBack();
				}}
			>
				Wróć do poprzedniej
			</Button>
		</Grid>
	);
};

export default NotFound;
