import React from 'react';
import ContentPage from '../../ui/ContentPage';
import CountriesContainer from '../../components/countries/Container';
import { getAllCountries } from '../../calls/countries';

const CountriesPage = props => {
	return (
		<ContentPage
			apiCall={getAllCountries}
			render={content => <CountriesContainer content={content} />}
		/>
	);
};

export default CountriesPage;
