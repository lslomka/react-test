import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { Public } from './public/Public';

const UnauthenticatedRoutes = () => {
	return (
		<BrowserRouter>
			<Switch>
				<Route component={Public} exact path='/' />
				<Route component={Public} exact path='/login' />
				<Route component={Public} exact path='/reset' />
				<Redirect to='/' />
			</Switch>
		</BrowserRouter>
	);
};

export default UnauthenticatedRoutes;
