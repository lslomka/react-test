import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import DashboardPage from './dashboard/Dashboard';
import FitterPage from './fitters/Page';
import CountriesPage from './countires/Countires';
import TopBar from '../ui/TopBar';
import NavMenu from '../ui/NavMenu';
import NotFound from './notFound/NotFound';
import { makeStyles, Box, Container, Grid } from '@material-ui/core';
import { ApplicationProvider } from '../contexts/application';

const useStyles = makeStyles(theme => ({
	wrapper: {
		display: 'flex',
		maxHeight: 'calc(100vh - 80px)',
		height: 'calc(100vh - 80px)'
	},
	content: {
		display: 'flex',
		flexDirection: 'column',
		paddingBottom: 10,
		width: '100%',
		margin: '0 auto'
	}
}));

const MainRoute = props => {
	const classes = useStyles();
	const Component = props.component;

	return (
		<Route
			render={route => {
				return (
					<>
						<ApplicationProvider>
							<TopBar {...route} />
							<Box className={classes.wrapper}>
								<NavMenu {...route} />
								{/* <Container className={classes.content}> */}
								<Grid className={classes.content}>
									<Component {...route} />
								</Grid>
								{/* </Container> */}
							</Box>
						</ApplicationProvider>
					</>
				);
			}}
		/>
	);
};

const AuthenticatedRoutes = props => {
	return (
		<BrowserRouter>
			<Switch>
				<MainRoute component={DashboardPage} exact path='/' />
				<MainRoute component={FitterPage} path='/fitters' />
				<MainRoute component={CountriesPage} exact path='/countries' />
				<Route component={NotFound} />
			</Switch>
		</BrowserRouter>
	);
};

export default AuthenticatedRoutes;
