import React from 'react';
import { Redirect, Route } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route
		{...rest}
		render={props =>
			isUserAuthenticated() ? (
				<Component {...props} />
			) : (
				<Redirect to='/login' />
			)
		}
	/>
);
