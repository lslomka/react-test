import React from 'react';
import { makeStyles, lighten } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import { NavLink as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import { useApplication } from '../contexts/application';
import { toggleDrawer } from '../actions/application';

const drawerWidth = 250;
const useStyles = makeStyles(theme => ({
	list: { overflowX: 'hidden', paddingLeft: 8 },
	fullList: {
		width: 'auto'
	},
	listItem: {
		'&:hover': {
			background: 'transparent',
			'& svg': {
				fill: lighten(theme.palette.primary.main, 0.2)
			},
			'& span': {
				color: lighten(theme.palette.primary.main, 0.2)
			}
		}
	},
	selectedLink: {
		'& svg': {
			fill: theme.palette.primary.main
		},
		'& span': {
			color: theme.palette.primary.main
		}
	},
	paper: {
		position: 'relative',
		background: 'transparent'
	},
	paperAnchorDockedLeft: {
		borderRight: 0
	},
	drawerOpen: {
		width: drawerWidth,
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	drawerClose: {
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		}),
		overflowX: 'hidden',
		width: theme.spacing(7) + 1,
		[theme.breakpoints.up('sm')]: {
			width: theme.spacing(9) + 1
		}
	}
}));

function ListItemLink(props) {
	const { icon, name, to } = props.item;
	const classes = useStyles();

	const renderLink = React.useMemo(
		() =>
			React.forwardRef((itemProps, ref) => (
				<RouterLink
					activeClassName={classes.selectedLink}
					exact
					to={to}
					ref={ref}
					{...itemProps}
				/>
			)),
		[classes.selectedLink, to]
	);

	return (
		<li>
			<ListItem button component={renderLink} className={classes.listItem}>
				{icon ? <ListItemIcon>{icon()}</ListItemIcon> : null}
				<ListItemText primary={name} />
			</ListItem>
		</li>
	);
}

const NavMenu = props => {
	const { data, dispatch } = useApplication();
	const classes = useStyles();
	const navItems = [
		{ id: 1, name: 'Dashboard', icon: () => <InboxIcon />, to: '/' },
		{ id: 2, name: 'Monterzy', icon: () => <InboxIcon />, to: '/fitters' },
		{ id: 3, name: 'Countries', icon: () => <InboxIcon />, to: '/countries' }
	];

	const handleDrawer = (side, openState) => event => {
		if (
			event.type === 'keydown' &&
			(event.key === 'Tab' || event.key === 'Shift')
		) {
			return;
		}

		dispatch(toggleDrawer(openState));
	};

	return (
		<React.Fragment>
			<Drawer
				variant='permanent'
				open={data.drawer}
				className={clsx(classes.drawer, {
					[classes.drawerOpen]: data.drawer,
					[classes.drawerClose]: !data.drawer
				})}
				classes={{
					paper: clsx(classes.paper, classes.paperAnchorDockedLeft, {
						[classes.drawerOpen]: data.drawer,
						[classes.drawerClose]: !data.drawer
					})
				}}
				onClose={handleDrawer(false)}>
				<List
					className={classes.list}
					onClick={handleDrawer(false)}
					onKeyDown={handleDrawer(false)}>
					{navItems.map((item, index) => (
						<ListItemLink item={item} key={item.id} />
					))}
				</List>
			</Drawer>
		</React.Fragment>
	);
};

export default NavMenu;
