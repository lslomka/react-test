import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import errorHandler from '../utils/errorHandler';
import { IconButton } from '@material-ui/core';

export default function ConfirmDialog(props) {
	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleConfirm = async () => {
		try {
			const res = await props.onConfirm();
			setOpen(false);
		} catch (err) {
			console.log(err);
			errorHandler(err);
		}
	};

	return (
		<>
			{props.buttonProps ? (
				<IconButton
					className={props.buttonProps.className}
					onClick={handleClickOpen}>
					{props.buttonProps.icon}
				</IconButton>
			) : (
				<Button
					variant='outlined'
					color='primary'
					onClick={handleClickOpen}>
					Open alert dialog
				</Button>
			)}
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby='confirm-dialog-title'
				aria-describedby='confirm-dialog-description'>
				<DialogTitle>{props.title}</DialogTitle>
				<DialogContent>
					<DialogContentText>{props.content}</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color='primary'>
						Anuluj
					</Button>
					<Button onClick={handleConfirm} color='primary' autoFocus>
						Potwierdź
					</Button>
				</DialogActions>
			</Dialog>
		</>
	);
}
