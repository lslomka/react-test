import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { MenuItem, Menu, Box } from '@material-ui/core';
import SearchInput from '../components/search/Input';
import { useApplication } from '../contexts/application';
import { toggleDrawer } from '../actions/application';

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1
	},
	menuButton: {
		marginRight: theme.spacing(2)
	},
	grow: {
		flexGrow: 1
	},
	appBar: {
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		})
	},
	appBarShift: {
		width: `calc(100% - ${drawerWidth}px)`,
		marginLeft: drawerWidth,
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	iconButton: {}
}));

const TopBar = props => {
	const classes = useStyles();
	const [anchorEl, setAnchorEl] = React.useState(null);
	const profileOpen = Boolean(anchorEl);
	const [openProfile, setOpenProfile] = React.useState(false);
	const { data, dispatch } = useApplication();

	const handleMenu = event => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const handleProfile = () => {
		setAnchorEl(null);
		setOpenProfile(true);
	};

	return (
		<div className={classes.root}>
			<AppBar
				position='static'
				color='transparent'
				elevation={0}
				className={clsx(classes.appBar)}
			>
				<Toolbar>
					<IconButton
						edge='start'
						className={classes.menuButton}
						color='primary'
						aria-label='menu'
						onClick={() => {
							dispatch(toggleDrawer(!data.drawer));
						}}
					>
						<MenuIcon />
					</IconButton>
					{/* <Box display={{ xs: 'none', md: 'block' }}>
						<Typography variant='h6'>ReactMonter</Typography>
					</Box> */}
					<Box className={classes.grow}></Box>
					<SearchInput />

					<Box className={classes.grow}></Box>
					<IconButton aria-label='notifications' color='primary'>
						<NotificationsIcon />
					</IconButton>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={handleMenu}
						color='primary'
					>
						<AccountCircle />
					</IconButton>
					<Menu
						id='menu-appbar'
						anchorEl={anchorEl}
						getContentAnchorEl={null}
						anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
						transformOrigin={{ vertical: 'top', horizontal: 'right' }}
						keepMounted
						open={profileOpen}
						onClose={handleClose}
					>
						<MenuItem onClick={handleProfile}>Profil</MenuItem>
						<MenuItem onClick={handleClose}>Wyloguj</MenuItem>
						{/* <Profile open={openProfile} /> */}
					</Menu>
				</Toolbar>
			</AppBar>
		</div>
	);
};

export default TopBar;
