import React from 'react';
import {
	Toolbar,
	IconButton,
	Tooltip,
	makeStyles,
	Box
} from '@material-ui/core';
import AppsIcon from '@material-ui/icons/Apps';
import ListIcon from '@material-ui/icons/List';
import TableChartIcon from '@material-ui/icons/TableChart';

const useStyles = makeStyles(theme => ({
	separator: {
		flexGrow: 1
	}
}));

const ContentToolbar = props => {
	const classes = useStyles();
	const { layout, setLayout, render, viewControls = true } = props;

	return (
		<Toolbar disableGutters style={{ padding: '0 5%' }}>
			{render ? render() : null}
			{viewControls && (
				<>
					<Box className={classes.separator} />
					<Tooltip title='Grid' arrow placement='top'>
						<IconButton
							color={layout === 'grid' ? 'primary' : 'default'}
							onClick={() => setLayout('grid')}>
							<AppsIcon />
						</IconButton>
					</Tooltip>
					<Tooltip title='Lista' arrow placement='top'>
						<IconButton
							color={layout === 'list' ? 'primary' : 'default'}
							onClick={() => setLayout('list')}>
							<ListIcon />
						</IconButton>
					</Tooltip>
					<Tooltip title='Tabela' arrow placement='top'>
						<IconButton
							color={layout === 'table' ? 'primary' : 'default'}
							onClick={() => setLayout('table')}>
							<TableChartIcon />
						</IconButton>
					</Tooltip>
				</>
			)}
		</Toolbar>
	);
};

export default ContentToolbar;
