import React from 'react';
import { Box, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
	root: {
		overflowY: 'auto',
		padding: '0 5%',
		height: '100%'
	}
}));

export function ScrollableContent(props) {
	const classes = useStyles();
	return <Box className={classes.root}>{props.children}</Box>;
}
