import React from 'react';
import UnauthenticatedRoutes from './pages/UnauthenticatedRoutes';

const UnauthenticatedApp = () => {
	return <UnauthenticatedRoutes />;
};

export default UnauthenticatedApp;
