import { createMuiTheme } from '@material-ui/core';
// import 'typeface-lato';
import 'typeface-roboto';

export const theme = createMuiTheme({
	typography: {
		fontFamily: [
			// 'Lato',
			'Roboto',
			'-apple-system',
			'BlinkMacSystemFont',
			'"Segoe UI"',
			'"Helvetica Neue"',
			'Arial',
			'sans-serif',
			'"Apple Color Emoji"',
			'"Segoe UI Emoji"',
			'"Segoe UI Symbol"'
		].join(',')
	},
	palette: {
		primary: {
			main: '#922c88'
		}
	}
});
