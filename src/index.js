import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { AuthProvider } from './contexts/auth';
import { UserProvider } from './contexts/user';

if (process.env.NODE_ENV === 'development') {
	const whyDidYouRender = require('@welldone-software/why-did-you-render');
	whyDidYouRender(React, {
		trackAllPureComponents: true
	});
}

ReactDOM.render(
	<AuthProvider>
		<UserProvider>
			<App />
		</UserProvider>
	</AuthProvider>,
	document.getElementById('root')
);
