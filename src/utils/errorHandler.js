import { ERROR_TYPES } from './errorTypes';
import { SERVER_ERROR_MESSAGES } from './errorMessages';

async function errorHandler(error) {
	let data = [],
		system = [],
		code,
		type;

	if (error.code && error.code === 'ECONNABORTED') {
		system.push({
			message: SERVER_ERROR_MESSAGES.timeout,
			stack: error.toJSON().stack
		});
		code = error.code;
		type = ERROR_TYPES.timeout;
	} else if (error.response) {
		const res = error.response;

		if (res.data) {
			if (res.data.SystemError) {
				system.push({
					message: res.data.SystemError.Exception.Error,
					stack: res.data.SystemError.Exception.Raw
				});
			}
			if (res.data.DataError) {
				data.push({
					message: res.data.DataError.Exception.Error,
					stack: res.data.DataError.Exception.Raw,
					member: res.data.DataError.Member
				});
			}
		} else {
			system.push({
				message: res.statusText,
				stack: error.toJSON().stack
			});
		}

		code = res.status;
		type = ERROR_TYPES.api;
	} else if (error.request) {
		const res = error.toJSON();

		system.push({
			message: res.message,
			stack: res.stack
		});

		code = ERROR_TYPES.server;
	} else {
		console.log(error.request);
		console.log(error.toJSON());
		// setType(ERROR_TYPES.client);
	}

	return { system, data, code, type };
}

export default errorHandler;
