export const ERROR_TYPES = {
	api: 'api',
	server: 'server',
	timeout: 'timeout',
	client: 'client'
};
