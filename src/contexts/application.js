import React, { useContext, useReducer } from 'react';
import { applicationReducer } from './../reducers/application';

const ApplicationContext = React.createContext();

const initialState = {
	drawer: false
};

const ApplicationProvider = props => {
	const [data, dispatch] = useReducer(applicationReducer, initialState);

	return (
		<ApplicationContext.Provider
			value={{
				data,
				dispatch
			}}>
			{props.children}
		</ApplicationContext.Provider>
	);
};

const useApplication = () => useContext(ApplicationContext);

export { ApplicationProvider, useApplication };
