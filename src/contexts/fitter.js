import React, { useState } from 'react';

const FitterContext = React.createContext();

const FitterProvider = props => {
	const [rowsPerPage, setRowsPerPage] = useState(20);
	const [page, setPage] = useState(0);
	const [totalCount, setTotalCount] = useState(0);
	const [hasMoreRecords, setHasMoreRecords] = useState(false);
	const [data, setData] = useState([]);
	const [filters, setFilters] = useState([]);
	const [sorters, setSorters] = useState([]);
	const [append, setAppend] = useState(false);
	const [pageDescriptor, setPageDescriptor] = useState({
		FilterDescriptors: [],
		SortDescriptors: [],
		IncludeTotalRowCount: true,
		Limit: rowsPerPage,
		Offset: page * rowsPerPage
	});

	const onDataFetch = resp => {
		console.log(append);
		setData(prev => {
			const newData = append
				? [...prev, ...resp.data.Fitters]
				: resp.data.Fitters;

			return newData;
		});

		if (totalCount !== resp.data.TotalCount)
			setTotalCount(resp.data.TotalCount);

		const doesHaveMore =
			pageDescriptor.Offset + pageDescriptor.Limit < resp.data.TotalCount;
		if (doesHaveMore !== hasMoreRecords) setHasMoreRecords(doesHaveMore);
		if (append) setAppend(!append);
	};

	const loadMore = () => {
		setAppend(true);
		setPage(page + 1);
		setPageDescriptor(prev => {
			return {
				...prev,
				Offset: page + rowsPerPage
			};
		});
	};

	const clear = () => {
		console.log('clear');
	};

	const updateFilters = () => {};

	const updateSorters = () => {};

	return (
		<FitterContext.Provider
			value={{
				data,
				setData,
				page,
				setPage,
				rowsPerPage,
				pageDescriptor,
				loadMore,
				hasMoreRecords,
				onDataFetch,
				clear
			}}
		>
			{props.children}
		</FitterContext.Provider>
	);
};

export { FitterProvider, FitterContext };
