import React, { useContext, useReducer } from 'react';
import { authReducer } from '../reducers/auth';
import { login } from './../calls/auth';

const AuthContext = React.createContext();

const initialState = {
	isAuthenticated: false,
	user: JSON.parse(localStorage.getItem('user'))
};

const AuthProvider = props => {
	const [data, dispatch] = useReducer(authReducer, initialState);

	const authLogin = data => {
		return login(data);
	};

	const authLogout = () => {
		console.log('logout');
	};

	return (
		<AuthContext.Provider
			value={{
				data,
				dispatch,
				authLogin,
				authLogout
			}}
		>
			{props.children}
		</AuthContext.Provider>
	);
};

const useAuth = () => useContext(AuthContext);

export { AuthProvider, useAuth };
