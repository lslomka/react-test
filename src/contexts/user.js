import React from 'react';
import { useAuth } from './';

const UserContext = React.createContext();

function UserProvider(props) {
	const {
		data: { user }
	} = useAuth();
	return <UserContext.Provider value={user} {...props} />;
}

function useUser() {
	return React.useContext(UserContext);
}

export { UserProvider, useUser };
