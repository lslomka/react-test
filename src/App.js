import React from 'react';
import { CssBaseline, ThemeProvider, makeStyles } from '@material-ui/core';
import { theme } from './styles/theme';
import { useUser } from './contexts/user';

//const loadAuthenticatedApp = () => import('./AuthenticatedApp');
const AuthenticatedApp = React.lazy(() => import('./AuthenticatedApp'));
const UnauthenticatedApp = React.lazy(() => import('./UnauthenticatedApp'));
const useStyles = makeStyles({
	root: {}
});

const FullPageSpinner = () => {
	return <div css={{ marginTop: '3em', fontSize: '4em' }}>SPINNER</div>;
};

function App() {
	//const classes = useStyles();
	//console.log(theme);
	const user = useUser();

	// React.useEffect(() => {
	// 	loadAuthenticatedApp();
	// }, []);

	return (
		<React.Fragment>
			<ThemeProvider theme={theme}>
				<CssBaseline />
				<React.Suspense fallback={<FullPageSpinner />}>
					{user ? <AuthenticatedApp /> : <UnauthenticatedApp />}
				</React.Suspense>
			</ThemeProvider>
		</React.Fragment>
	);
}

export default App;
