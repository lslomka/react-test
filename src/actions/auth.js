export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGOUT = 'LOGOUT';

export const loginSuccess = data => {
	const user = data.data;
	return {
		type: LOGIN_SUCCESS,
		payload: { isAuthenticated: true, user }
	};
};

export const loginError = data => {
	return {
		type: LOGIN_ERROR,
		payload: data
	};
};
