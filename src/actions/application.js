export const TOGGLE_DRAWER = 'TOGGLE_DRAWER';

export const toggleDrawer = data => {
	return {
		type: TOGGLE_DRAWER,
		payload: { drawer: data }
	};
};
