import React, { useState } from 'react';
import {
	TextField,
	Button,
	Grid,
	makeStyles,
	LinearProgress
} from '@material-ui/core';
import { useAuth } from './../../contexts/auth';
import { green } from '@material-ui/core/colors';
import clsx from 'clsx';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { NavLink } from 'react-router-dom';

const initialState = {
	login: '',
	password: ''
};

const useStyles = makeStyles(theme => ({
	buttonSuccess: {
		backgroundColor: green[500],
		'&:hover': {
			backgroundColor: green[700]
		}
	},
	buttonProgress: {
		color: green[500],
		position: 'absolute',
		top: '56%',
		left: '50%',
		marginLeft: -12
	},
	submit: {
		margin: theme.spacing(5, 0, 2)
	}
}));

const Reset = props => {
	const { state, dispatch, authLogin } = useAuth();
	const [loading, setLoading] = useState(false);
	const [success, setSuccess] = useState(false);
	const classes = useStyles();

	const buttonClassname = clsx({
		[classes.buttonSuccess]: success
	});

	return (
		<>
			<Formik
				initialValues={initialState}
				onSubmit={(values, actions) => {
					const { login, password } = values;

					actions.setSubmitting(true);

					authLogin({ login, password })
						.then(res => {
							console.log(res);
						})
						.catch(err => {
							const res = err.response;

							if (res.status === 401)
								actions.setErrors({
									login: 'Nieprawidłowy login lub hasło',
									password: 'Nieprawidłowy login lub hasło'
								});
						})
						.finally(() => {
							setTimeout(function() {
								actions.setSubmitting(false);
							}, 2000);
						});
				}}
				validationSchema={Yup.object().shape({
					login: Yup.string().required('Pole wymagane, uzupełnij dane...'),
					password: Yup.string().required(
						'Pole wymagane, uzupełnij dane...'
					)
				})}>
				{props => {
					const {
						values,
						touched,
						errors,
						isSubmitting,
						handleChange,
						handleBlur,
						handleSubmit
					} = props;

					return (
						<form noValidate autoComplete='off' onSubmit={handleSubmit}>
							{isSubmitting && <LinearProgress />}
							<TextField
								autoFocus
								value={values.login}
								label='Login'
								name='login'
								margin='normal'
								fullWidth
								onChange={handleChange}
								error={errors.hasOwnProperty('login')}
								helperText={errors.login}
							/>
							<ErrorMessage name='login' />
							<TextField
								value={values.password}
								label='Hasło'
								type='password'
								name='password'
								margin='normal'
								fullWidth
								onChange={handleChange}
								error={errors.hasOwnProperty('password')}
								helperText={errors.password}
							/>
							<Button
								variant='contained'
								color='primary'
								className={`${buttonClassname} ${classes.submit}`}
								disabled={isSubmitting}
								type='submit'
								fullWidth>
								Zaloguj
							</Button>
						</form>
					);
				}}
			</Formik>
			<Grid container>
				<Grid item xs>
					<NavLink to='/reset' variant='body2'>
						Zapomniałeś hasło?
					</NavLink>
				</Grid>
			</Grid>
		</>
	);
};

export default Reset;
