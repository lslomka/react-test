import React from 'react';
import {
	TextField,
	Button,
	Grid,
	makeStyles,
	LinearProgress
} from '@material-ui/core';
import { useAuth } from './../../contexts/auth';
import { green } from '@material-ui/core/colors';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { NavLink } from 'react-router-dom';
import { loginSuccess, loginError } from '../../actions/auth';
import { useIsMounted } from '../../hooks';

const initialState = {
	login: '',
	password: ''
};

const useStyles = makeStyles(theme => ({
	buttonSuccess: {
		backgroundColor: green[500],
		'&:hover': {
			backgroundColor: green[700]
		}
	},
	buttonProgress: {
		color: green[500],
		position: 'absolute',
		top: '56%',
		left: '50%',
		marginLeft: -12
	},
	submit: {
		margin: theme.spacing(5, 0, 2)
	}
}));

const Login = props => {
	const { dispatch, authLogin } = useAuth();
	const classes = useStyles();
	const isMounted = useIsMounted();

	const formSubmit = (values, actions) => {
		const { login, password } = values;

		actions.setSubmitting(true);

		authLogin({ login, password })
			.then(res => {
				dispatch(loginSuccess(res));
				window.location.reload();
			})
			.catch(err => {
				const res = err.response;

				dispatch(loginError(err));

				if (res.status === 401)
					actions.setErrors({
						login: 'Nieprawidłowy login lub hasło',
						password: 'Nieprawidłowy login lub hasło'
					});
			})
			.finally(() => {
				if (isMounted.current) actions.setSubmitting(false);
			});
	};

	return (
		<>
			<Formik
				initialValues={initialState}
				onSubmit={(values, actions) => formSubmit(values, actions)}
				validationSchema={Yup.object().shape({
					login: Yup.string().required('Pole wymagane, uzupełnij dane...'),
					password: Yup.string().required(
						'Pole wymagane, uzupełnij dane...'
					)
				})}>
				{props => {
					const {
						values,
						errors,
						touched,
						isSubmitting,
						handleChange,
						handleSubmit
					} = props;

					return (
						<form noValidate autoComplete='off' onSubmit={handleSubmit}>
							{isSubmitting && <LinearProgress />}
							<TextField
								autoFocus
								value={values.login}
								label='Login'
								name='login'
								margin='normal'
								fullWidth
								onChange={handleChange}
								error={errors.hasOwnProperty('login') && touched.login}
								helperText={touched.login && errors.login}
							/>
							<TextField
								value={values.password}
								label='Hasło'
								type='password'
								name='password'
								margin='normal'
								fullWidth
								onChange={handleChange}
								error={
									errors.hasOwnProperty('password') && touched.password
								}
								helperText={touched.password && errors.password}
							/>
							<Button
								variant='contained'
								color='primary'
								className={`${classes.submit}`}
								disabled={isSubmitting}
								type='submit'
								fullWidth>
								Zaloguj
							</Button>
						</form>
					);
				}}
			</Formik>
			<Grid container>
				<Grid item xs>
					<NavLink to='/reset' variant='body2'>
						Zapomniałeś hasło?
					</NavLink>
				</Grid>
			</Grid>
		</>
	);
};

export default Login;
