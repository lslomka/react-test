import React from 'react';
import {
	Dialog,
	DialogTitle,
	DialogContent,
	DialogContentText,
	DialogActions,
	Button,
	Paper,
	makeStyles,
	IconButton
} from '@material-ui/core';
import Draggable from 'react-draggable';
import GetAppIcon from '@material-ui/icons/GetApp';

const useStyles = makeStyles({
	actions: {
		justifyContent: 'space-between'
	}
});

const ErrorDialog = props => {
	const [open, setOpen] = React.useState(true);
	const { error } = props;
	const classes = useStyles();

	const message = error.system[0].message;
	const stack = error.system[0].stack;

	function PaperComponent(props) {
		return (
			<Draggable
				handle='#draggable-dialog-title'
				cancel={'[class*="MuiDialogContent-root"]'}>
				<Paper {...props} />
			</Draggable>
		);
	}

	const handleClose = () => {
		setOpen(false);
	};

	const handleError = () => {};

	handleError();

	return (
		<Dialog
			open={open}
			onClose={handleClose}
			disableEscapeKeyDown
			disableBackdropClick
			PaperComponent={PaperComponent}
			aria-labelledby='draggable-dialog-title'>
			<DialogTitle style={{ cursor: 'move' }} id='draggable-dialog-title'>
				Tytuł
			</DialogTitle>
			<DialogContent>
				<DialogContentText>
					{message}
					<br />
					{stack}
				</DialogContentText>
			</DialogContent>
			<DialogActions className={classes.actions}>
				<IconButton onClick={handleClose} color='primary'>
					<GetAppIcon />
				</IconButton>
				<Button onClick={handleClose} color='primary'>
					Rozumiem
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default ErrorDialog;
