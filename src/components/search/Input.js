import React from 'react';
import { IconButton, InputBase } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

const SearchInput = () => {
	return (
		<React.Fragment>
			<IconButton aria-label='search icon'>
				<SearchIcon />
			</IconButton>
			<InputBase
				placeholder='Szukaj...'
				inputProps={{ 'aria-label': 'Szukaj w aplikacji' }}
			/>
		</React.Fragment>
	);
};

export default SearchInput;
