import React from 'react';
import {
	List,
	ListItem,
	ListItemAvatar,
	Avatar,
	ListItemText,
	Typography
} from '@material-ui/core';

const CountriesList = props => {
	return (
		<List component='nav' className={props.className}>
			{props.data.map(country => {
				return (
					<ListItem button key={country.numericCode}>
						<ListItemAvatar>
							<Avatar alt={country.name} src={country.flag} />
						</ListItemAvatar>
						<ListItemText
							primary={country.name}
							secondary={
								<React.Fragment>
									<Typography
										component='span'
										variant='body2'
										color='textPrimary'
									>
										Capital: {country.capital}
									</Typography>
								</React.Fragment>
							}
						/>
					</ListItem>
				);
			})}
		</List>
	);
};

export default CountriesList;
