import React, { useState } from 'react';
import { Toolbar, Button, Typography, LinearProgress } from '@material-ui/core';
import ErrorDialog from '../error/ErrorDialog';
import { useAsync } from '../../hooks';
import CountriesContainer from './CountriesContainer';
import { getAllCountries } from '../../calls/countries';

const CountriesPage = props => {
	const [layout, setLayout] = useState('grid');
	const { pending, value, error } = useAsync(getAllCountries, true);

	const parseData = () => {
		value.data = value.data.splice(0, 20);
		return value;
	};

	return (
		<React.Fragment>
			<Toolbar>
				<Button onClick={() => setLayout('grid')}>Grid</Button>
				<Button onClick={() => setLayout('list')}>List</Button>
				<Button onClick={() => setLayout('table')}>Table</Button>
			</Toolbar>

			{pending ? (
				<LinearProgress />
			) : error ? (
				<ErrorDialog message={error} />
			) : !value || !value.data || !value.data.length ? (
				<Typography>Brak wyników</Typography>
			) : (
				<CountriesContainer
					value={parseData()}
					pending={pending}
					error={error}
					layout={layout}
				/>
			)}
		</React.Fragment>
	);
};

export default CountriesPage;
