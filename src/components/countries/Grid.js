import React from 'react';
import { Grid } from '@material-ui/core';
import CountryCard from './Card';

const CountriesGrid = props => {
	return (
		<Grid
			container
			spacing={2}
			direction='row'
			justify='flex-start'
			alignItems='flex-start'
			className={props.className}
		>
			{props.data.map(country => {
				return (
					<Grid item xs={12} sm={6} md={3} key={country.numericCode}>
						<CountryCard country={country} />
					</Grid>
				);
			})}
		</Grid>
	);
};

export default CountriesGrid;
