import React from 'react';
import {
	Card,
	CardActionArea,
	CardMedia,
	makeStyles,
	Typography,
	CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
	root: {},
	media: {
		height: 150
	}
});

const CountryCard = ({ country }) => {
	const classes = useStyles();
	return (
		<Card className={classes.root}>
			<CardActionArea>
				<CardMedia
					className={classes.media}
					image={country.flag}
					title={country.name}
				/>
				<CardContent>
					<Typography variant='h6'>{country.name}</Typography>
					<Typography variant='subtitle1'>
						Capital: {country.capital}
					</Typography>
				</CardContent>
			</CardActionArea>
		</Card>
	);
};

export default CountryCard;
