import React from 'react';
import { makeStyles } from '@material-ui/core';
import CountriesGrid from './Grid';
import CountriesList from './List';

const useStyles = makeStyles(theme => ({
	content: {
		overflowY: 'auto'
	}
}));

const CountriesContainer = props => {
	const { value, layout } = props.content;
	const classes = useStyles();

	if (layout === 'table')
		return <CountriesGrid data={value.data} className={classes.content} />;

	if (layout === 'grid')
		return <CountriesGrid data={value.data} className={classes.content} />;

	if (layout === 'list')
		return <CountriesList data={value.data} className={classes.content} />;
};

export default CountriesContainer;
