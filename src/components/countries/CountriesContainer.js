import React from 'react';
import CountryCard from './CountryCard';
import {
	Grid,
	Typography,
	List,
	ListItem,
	ListItemAvatar,
	ListItemText,
	Avatar
} from '@material-ui/core';

const CountriesContainer = ({ pending, value, error, layout }) => {
	const countries = value.data;

	if (layout === 'table') return <div>Nie ma</div>;
	if (layout === 'grid')
		return (
			<Grid
				container
				spacing={2}
				direction='row'
				justify='flex-start'
				alignItems='flex-start'>
				{countries.map(country => {
					return (
						<Grid item xs={12} sm={6} md={3} key={country.numericCode}>
							<CountryCard country={country} />
						</Grid>
					);
				})}
			</Grid>
		);

	if (layout === 'list')
		return (
			<List component='nav'>
				{countries.map(country => {
					return (
						<ListItem button key={country.numericCode}>
							<ListItemAvatar>
								<Avatar alt={country.name} src={country.flag} />
							</ListItemAvatar>
							<ListItemText
								primary={country.name}
								secondary={
									<React.Fragment>
										<Typography
											component='span'
											variant='body2'
											// className={classes.inline}
											color='textPrimary'>
											Capital: {country.capital}
										</Typography>
									</React.Fragment>
								}
							/>
						</ListItem>
					);
				})}
			</List>
		);
};

export default CountriesContainer;
