import React, { useEffect, useCallback, useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {
	Fade,
	makeStyles,
	LinearProgress,
	FormControlLabel,
	Switch,
	FormGroup,
	Collapse
} from '@material-ui/core';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { green } from '@material-ui/core/colors';
import { getFitter, addFitter, putFitter } from '../../../calls/fitters';
import errorHandler from '../../../utils/errorHandler';
import ErrorDialog from '../../error/Dialog';

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Fade in={true} ref={ref} {...props} />;
});

const useStyles = makeStyles(theme => ({
	buttonProgress: {
		color: green[500],
		position: 'absolute',
		right: '9%'
	},
	linearProgress: {
		width: '100%',
		position: 'absolute',
		bottom: 0,
		left: 0,
		margin: 0
	}
}));

const AddEditFitter = props => {
	const classes = useStyles();
	const id = !isNaN(Number(props.match.params.id))
		? props.match.params.id
		: null;
	const [open, setOpen] = useState(true);
	const [initValues, setInitValues] = useState({
		Name: '',
		Login: '',
		HashedPassword: '',
		IsActive: true,
		ChangePassword: false
	});
	const [pending, setPending] = useState(false);
	const [error, setError] = useState(null);

	// const handleClickOpen = () => {
	// 	setOpen(true);
	// };

	const handleClose = () => {
		setOpen(false);
		props.history.goBack();
	};

	const fetchData = useCallback(async id => {
		setPending(true);

		try {
			const resp = await getFitter(id);
			setInitValues(prev => {
				resp.data.Fitter.HashedPassword = '';
				return { ...prev, ...resp.data.Fitter };
			});
		} catch (err) {
			setError(await errorHandler(err));
		}

		setPending(false);
	}, []);

	const saveData = data => {
		const method = id ? putFitter : addFitter;

		return method(data, id);
	};

	useEffect(() => {
		if (id) fetchData(id);
	}, []);

	return (
		<>
			{pending ? (
				<LinearProgress />
			) : error ? (
				<ErrorDialog error={error} />
			) : (
				<Dialog
					open={open}
					onClose={handleClose}
					aria-labelledby='form-dialog-title'
					TransitionComponent={Transition}>
					<Formik
						initialValues={initValues}
						onSubmit={async (values, actions) => {
							actions.setSubmitting(true);

							try {
								const resp = await saveData(values);
								handleClose();
							} catch (err) {
								const errors = await errorHandler(err);

								setError(errors);

								if (errors.data.length) {
									errors.data.forEach(de => {
										actions.setFieldError(de.member, de.message);
									});
								}
							}
						}}
						validationSchema={Yup.object().shape({
							Name: Yup.string()
								.min(3, 'Nazwa musi posiadać co najmniej 3 znaki')
								.max(20, 'Nazwa nie może być dłuższa niż 20 znaków')
								.required('Pole wymagane, uzupełnij dane...'),
							Login: Yup.string()
								.min(3, 'Login musi posiadać co najmniej 3 znaki')
								.max(20, 'Login nie może być dłuższy niż 20 znaków')
								.required('Pole wymagane, uzupełnij dane...'),
							HashedPassword: Yup.string()
								.when('ChangePassword', {
									is: true,
									then: Yup.string().required(
										'Pole wymagane, uzupełnij dane...'
									),
									otherwise: Yup.string().notRequired()
								})
								//.required('Pole wymagane, uzupełnij dane...')
								.nullable()
								.matches(
									/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
									{
										message:
											'8 znaków, musi zawierać conajmniej dużą i małą literę, liczbę oraz znak specjalny'
									}
								)
						})}>
						{props => {
							const {
								values,
								touched,
								errors,
								isSubmitting,
								handleChange,
								handleBlur,
								handleSubmit
							} = props;

							return (
								<>
									{isSubmitting && (
										<LinearProgress
											className={classes.linearProgress}
										/>
									)}
									<DialogTitle id='form-dialog-title'>
										Dodaj montera
									</DialogTitle>
									<DialogContent>
										<form onSubmit={handleSubmit}>
											<TextField
												autoFocus
												margin='normal'
												name='Name'
												label='Nazwa'
												type='text'
												value={values.Name}
												onChange={handleChange}
												onBlur={handleBlur}
												error={errors.Name && touched.Name}
												helperText={
													errors.Name &&
													touched.Name &&
													errors.Name
												}
												fullWidth
											/>
											<TextField
												margin='normal'
												name='Login'
												label='Login'
												type='text'
												fullWidth
												value={values.Login}
												onChange={handleChange}
												onBlur={handleBlur}
												error={errors.Login && touched.Login}
												helperText={
													errors.Login &&
													touched.Login &&
													errors.Login
												}
											/>
											{!id && (
												<TextField
													margin='normal'
													name='HashedPassword'
													label='Hasło'
													type='password'
													fullWidth
													value={values.HashedPassword}
													onChange={handleChange}
													onBlur={handleBlur}
													error={
														errors.HashedPassword &&
														touched.HashedPassword
													}
													helperText={
														errors.HashedPassword &&
														touched.HashedPassword &&
														errors.HashedPassword
													}
												/>
											)}
											<FormGroup>
												<FormControlLabel
													control={
														<Switch
															name='IsActive'
															checked={values.IsActive}
															value={values.IsActive}
															onChange={handleChange}
															color='primary'
															size='small'
														/>
													}
													label={
														values.IsActive
															? 'Aktywny'
															: 'Nieaktywny'
													}
												/>
												<FormControlLabel
													control={
														<Switch
															name='ChangePassword'
															checked={values.ChangePassword}
															value={values.ChangePassword}
															onChange={handleChange}
															color='primary'
															size='small'
														/>
													}
													label='Zmień hasło'
												/>
												{id && values.ChangePassword && (
													<Collapse in={values.ChangePassword}>
														<TextField
															margin='normal'
															name='HashedPassword'
															label='Hasło'
															type='password'
															fullWidth
															value={values.HashedPassword}
															onChange={handleChange}
															onBlur={handleBlur}
															error={
																values.ChangePassword &&
																errors.HashedPassword &&
																touched.HashedPassword
															}
															helperText={
																values.ChangePassword &&
																errors.HashedPassword &&
																touched.HashedPassword &&
																errors.HashedPassword
															}
														/>
													</Collapse>
												)}
											</FormGroup>
										</form>
									</DialogContent>
									<DialogActions>
										<Button
											onClick={handleClose}
											color='primary'
											disabled={isSubmitting}>
											Anuluj
										</Button>
										<Button
											type='submit'
											disabled={isSubmitting}
											onClick={handleSubmit}
											color='primary'>
											Dodaj
										</Button>
									</DialogActions>
								</>
							);
						}}
					</Formik>
				</Dialog>
			)}
		</>
	);
};

export default AddEditFitter;
