import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import {
	makeStyles,
	LinearProgress,
	Grid,
	Typography,
	Button,
	Paper,
	IconButton
} from '@material-ui/core';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { green } from '@material-ui/core/colors';
import { addFitter } from '../../../calls/fitters';
import errorHandler from '../../../utils/errorHandler';
import ErrorDialog from '../../error/Dialog';
import ContentToolbar from '../../../ui/ContentToolbar';
import { ScrollableContent } from '../../../ui/ScrollableContent';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useHistory, useLocation, Redirect } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
	buttonProgress: {
		color: green[500],
		position: 'absolute',
		right: '9%'
	},
	linearProgress: {
		width: '100%',
		position: 'absolute',
		bottom: 0,
		left: 0,
		margin: 0
	},
	submit: {
		margin: theme.spacing(3, 0, 0, 0)
	},
	paperRoot: {
		padding: 40
	},
	buttonsBar: {
		display: 'flex',
		justifyContent: 'flex-end'
	}
}));

const AddFitter = props => {
	const classes = useStyles();
	const history = useHistory();
	const [redirectTarget, setRedirectTarget] = useState('/fitters');
	const [redirect, setRedirect] = useState(false);
	const [error, setError] = useState(null);
	const initValues = {
		Name: '',
		Login: '',
		HashedPassword: ''
	};

	const renderRedirect = () => {
		if (redirect) {
			return <Redirect to={redirectTarget} />;
		}
	};

	const handleClose = () => {
		//history.goBack();
		setRedirect(true);
	};

	return (
		<>
			{renderRedirect()}
			{error ? (
				<ErrorDialog error={error} />
			) : (
				<>
					<ContentToolbar
						width='100%'
						viewControls={false}
						render={() => {
							return (
								<IconButton onClick={handleClose}>
									<ArrowBackIcon />
								</IconButton>
							);
						}}
					/>
					<ScrollableContent>
						<Grid
							container
							spacing={0}
							direction='row'
							justify='center'
							//alignItems='stretch'
						>
							<Grid item xs={12} sm={6}>
								<Paper className={classes.paperRoot} elevation={1}>
									<Typography variant='h5' color='primary'>
										Nowy monter
									</Typography>
									<Formik
										initialValues={initValues}
										onSubmit={async (values, actions) => {
											actions.setSubmitting(true);

											try {
												const resp = await addFitter(values);
												console.log(resp);
												handleClose();
											} catch (err) {
												const errors = await errorHandler(err);
												console.log(errors);
												if (errors.data.length) {
													errors.data.forEach(de => {
														actions.setFieldError(
															de.member,
															de.message
														);
													});
												} else {
													console.log(errors);
													setError(errors);
												}
											}
										}}
										validationSchema={Yup.object().shape({
											Name: Yup.string()
												.min(
													3,
													'Nazwa musi posiadać co najmniej 3 znaki'
												)
												.max(
													20,
													'Nazwa nie może być dłuższa niż 20 znaków'
												)
												.required(
													'Pole wymagane, uzupełnij dane...'
												),
											Login: Yup.string()
												.min(
													2,
													'Login musi posiadać co najmniej 3 znaki'
												)
												.max(
													20,
													'Login nie może być dłuższy niż 20 znaków'
												)
												.required(
													'Pole wymagane, uzupełnij dane...'
												),
											HashedPassword: Yup.string()
												.required(
													'Pole wymagane, uzupełnij dane...'
												)
												.nullable()
												.matches(
													/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
													{
														message:
															'8 znaków, musi zawierać conajmniej dużą i małą literę, liczbę oraz znak specjalny'
													}
												)
										})}
									>
										{props => {
											const {
												values,
												touched,
												errors,
												isSubmitting,
												handleChange,
												handleBlur,
												handleSubmit
											} = props;

											return (
												<>
													{isSubmitting && (
														<LinearProgress
															className={classes.linearProgress}
														/>
													)}
													<form onSubmit={handleSubmit}>
														<TextField
															autoFocus
															margin='normal'
															name='Name'
															label='Nazwa'
															type='text'
															value={values.Name}
															onChange={handleChange}
															onBlur={handleBlur}
															error={errors.Name && touched.Name}
															helperText={
																errors.Name &&
																touched.Name &&
																errors.Name
															}
															fullWidth
														/>
														<TextField
															margin='normal'
															name='Login'
															label='Login'
															type='text'
															fullWidth
															value={values.Login}
															onChange={handleChange}
															onBlur={handleBlur}
															error={
																errors.Login && touched.Login
															}
															helperText={
																errors.Login &&
																touched.Login &&
																errors.Login
															}
														/>
														<TextField
															margin='normal'
															name='HashedPassword'
															label='Hasło'
															type='password'
															fullWidth
															value={values.HashedPassword}
															onChange={handleChange}
															onBlur={handleBlur}
															error={
																errors.HashedPassword &&
																touched.HashedPassword
															}
															helperText={
																errors.HashedPassword &&
																touched.HashedPassword &&
																errors.HashedPassword
															}
														/>
														<div className={classes.buttonsBar}>
															<Button
																variant='contained'
																color='primary'
																className={`${classes.submit}`}
																disabled={isSubmitting}
																type='submit'
															>
																Dodaj
															</Button>
														</div>
													</form>
												</>
											);
										}}
									</Formik>
								</Paper>
							</Grid>
						</Grid>
					</ScrollableContent>
				</>
			)}
		</>
	);
};

export default AddFitter;
