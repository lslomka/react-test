import React from 'react';
import {
	List,
	ListItem,
	ListItemAvatar,
	Avatar,
	ListItemText,
	Typography,
	makeStyles,
	Divider
} from '@material-ui/core';
import { useFitter } from '../../../hooks';
import { grey } from '@material-ui/core/colors';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
	avatarInactive: {
		backgroundColor: grey[200],
		color: theme.palette.getContrastText(grey[200])
	},
	avatarActive: {
		backgroundColor: theme.palette.primary.light,
		color: theme.palette.getContrastText(theme.palette.primary.light)
	}
}));

const FittersList = props => {
	const classes = useStyles();
	const { data } = useFitter(false);

	return (
		<List className={props.className}>
			{data.map(fitter => {
				return (
					<React.Fragment key={fitter.Id}>
						<ListItem button dense divider>
							<ListItemAvatar>
								<Avatar
									aria-label='recipe'
									className={clsx({
										[classes.avatarActive]: fitter.IsActive,
										[classes.avatarInactive]: !fitter.IsActive
									})}
								>
									{`${fitter.Name[0]}`}
								</Avatar>
							</ListItemAvatar>
							<ListItemText
								primary={fitter.Name}
								secondary={fitter.Login}
							/>
							<ListItemText
								primary={`Zlecone ${fitter.TotalNumber}`}
								primaryTypographyProps={{
									component: 'span',
									variant: 'body2'
								}}
							/>
							<ListItemText
								primary={`Wykonane ${fitter.Mountednumber}`}
								primaryTypographyProps={{
									component: 'span',
									variant: 'body2'
								}}
							/>
							<ListItemText
								primary={`Problemy ${fitter.ErrorNumber}`}
								primaryTypographyProps={{
									component: 'span',
									variant: 'body2'
								}}
							/>
						</ListItem>
					</React.Fragment>
				);
			})}
		</List>
	);
};

export default FittersList;
