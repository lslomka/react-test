import React from 'react';
import { Grid } from '@material-ui/core';
import FitterGridCard from './GridCard';
import { useFitter } from '../../../hooks';

const FittersGrid = props => {
	const { data } = useFitter(false);
	return (
		<Grid
			container
			spacing={2}
			direction='row'
			justify='flex-start'
			alignItems='flex-start'
			className={props.className}
		>
			{data.map(fitter => {
				return (
					<Grid item xs={12} sm={6} md={4} key={fitter.Id}>
						<FitterGridCard fitter={fitter} />
					</Grid>
				);
			})}
		</Grid>
	);
};

export default FittersGrid;
