import React, { useState } from 'react';
import {
	Card,
	makeStyles,
	CardContent,
	CardHeader,
	Avatar,
	IconButton,
	FormControlLabel,
	Switch,
	Chip,
	Menu,
	MenuItem
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { grey } from '@material-ui/core/colors';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
	root: {},
	media: {
		height: 150
	},
	avatarInactive: {
		backgroundColor: grey[200],
		color: theme.palette.getContrastText(grey[200])
	},
	avatarActive: {
		backgroundColor: theme.palette.primary.light,
		color: theme.palette.getContrastText(theme.palette.primary.light)
	},
	chipsRow: {
		display: 'flex',
		justifyContent: 'space-evenly'
	}
}));

const FitterGridCard = ({ fitter }) => {
	const classes = useStyles();
	const [anchorEl, setAnchorEl] = useState(null);
	const [elevation, setElevation] = useState(1);
	const profileOpen = Boolean(anchorEl);

	const handleMenuOpen = e => {
		setAnchorEl(e.currentTarget);
	};

	const handleMenuClose = () => {
		setAnchorEl(null);
	};

	const handleChange = e => {
		console.log(e);
	};

	return (
		<Card
			className={classes.root}
			elevation={elevation}
			onMouseEnter={() => setElevation(5)}
			onMouseLeave={() => setElevation(1)}
		>
			<CardHeader
				avatar={
					<Avatar
						aria-label='recipe'
						className={clsx({
							[classes.avatarActive]: fitter.IsActive,
							[classes.avatarInactive]: !fitter.IsActive
						})}
					>
						{`${fitter.Name[0]}`}
					</Avatar>
				}
				action={
					<React.Fragment>
						<IconButton
							aria-label='settings'
							aria-controls='fitter-settings'
							aria-haspopup='true'
							onClick={handleMenuOpen}
						>
							<MoreVertIcon />
						</IconButton>
						<Menu
							id='fitter-settings'
							anchorEl={anchorEl}
							getContentAnchorEl={null}
							anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
							transformOrigin={{ vertical: 'top', horizontal: 'right' }}
							keepMounted
							open={profileOpen}
							onClose={handleMenuClose}
						>
							<MenuItem>Edytuj</MenuItem>
							<MenuItem>Usuń</MenuItem>
							<MenuItem>
								<FormControlLabel
									control={
										<Switch
											checked={fitter.IsActive}
											onChange={handleChange}
											size='small'
											color='primary'
										/>
									}
									label={fitter.IsActive ? 'Aktywny' : 'Nieaktywny'}
								/>
							</MenuItem>
						</Menu>
					</React.Fragment>
				}
				title={fitter.Name}
				subheader={fitter.Login}
			/>
			<CardContent className={classes.chipsRow}>
				<Chip
					label={`Zlecone - ${fitter.TotalNumber}`}
					variant='outlined'
				/>
				<Chip
					label={`Wykonane - ${fitter.Mountednumber}`}
					color='primary'
					variant='outlined'
				/>
				<Chip
					label={`Problemy - ${fitter.ErrorNumber}`}
					color='secondary'
					variant='outlined'
				/>
			</CardContent>
		</Card>
	);
};

export default FitterGridCard;
