import React from 'react';
import {
	makeStyles,
	Button,
	Backdrop,
	CircularProgress,
	Box,
	Typography,
	IconButton,
	Tooltip
} from '@material-ui/core';
import FittersTable from './table/Table';
import FittersGrid from './grid/Grid';
import FittersList from './list/List';
import { useFitter } from './../../hooks';
import ContentToolbar from '../../ui/ContentToolbar';
import ErrorDialog from '../error/Dialog';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { Link as RouterLink } from 'react-router-dom';
import { ScrollableContent } from '../../ui/ScrollableContent';

const useStyles = makeStyles(theme => ({
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: '#fff'
	},
	title: {
		marginRight: theme.spacing(1)
	}
}));

const FittersContainer = () => {
	const classes = useStyles();
	const [layout, setLayout] = React.useState('table');
	const { pending, error, loadMore, hasMoreRecords } = useFitter();

	return (
		<>
			<ContentToolbar
				layout={layout}
				setLayout={setLayout}
				render={() => {
					return (
						<>
							<Typography className={classes.title} variant='h6'>
								Monterzy
							</Typography>
							<Tooltip title='Dodaj' arrow placement='right'>
								<IconButton
									color='primary'
									component={RouterLink}
									to={`/fitters/new`}
								>
									<AddCircleOutlineIcon />
								</IconButton>
							</Tooltip>
						</>
					);
				}}
			/>
			<Backdrop className={classes.backdrop} open={pending}>
				<CircularProgress />
			</Backdrop>
			{error && <ErrorDialog error={error} />}
			<ScrollableContent>
				{layout === 'table' && <FittersTable />}
				{layout === 'grid' && <FittersGrid />}
				{layout === 'list' && <FittersList />}
				{hasMoreRecords && (
					<Button onClick={loadMore}>Wczytaj więcej...</Button>
				)}
			</ScrollableContent>
		</>
	);
};

export default FittersContainer;
