import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { IconButton, makeStyles } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { useFitter } from '../../../hooks';
import { Link as RouterLink } from 'react-router-dom';
import ConfirmDialog from '../../../ui/ConfirmDialog';
import { deleteFitter } from '../../../calls/fitters';

const useStyles = makeStyles(theme => ({
	tablePagination: {
		overflow: 'visible'
	},
	spacer: {
		flexGrow: 1
	},
	actionButton: {
		padding: 8
	}
}));

const columns = [
	{ id: 'Name', label: 'Nazwa', minWidth: 170 },
	{ id: 'Login', label: 'Login', minWidth: 100 },
	{
		id: 'IsActive',
		label: 'Aktywny',
		format: value => {
			return value ? 'Tak' : 'Nie';
		}
	},
	{
		id: 'TotalNumber',
		label: 'Zlecone',
		format: value => value.toLocaleString()
	},
	{
		id: 'Mountednumber',
		label: 'Wykonane',
		format: value => value.toLocaleString()
	},
	{
		id: 'ErrorNumber',
		label: 'Problemy',
		format: value => value.toLocaleString()
	},
	{ id: 'Actions', label: 'Akcje' }
];

const FittersTable = () => {
	const classes = useStyles();
	const { data } = useFitter(false);

	const onRowMouseEnter = (e, id) => {
		//console.log(e, id);
	};

	const createColumns = cols => {
		return cols.map(column => (
			<TableCell
				key={column.id}
				align={column.align}
				style={{ minWidth: column.minWidth }}
			>
				{column.label}
			</TableCell>
		));
	};

	const createRows = (rows, columns) => {
		return rows.map(row => {
			return (
				<TableRow
					hover
					onMouseEnter={e => {
						return onRowMouseEnter(e, row.Id);
					}}
					role='checkbox'
					tabIndex={-1}
					key={row.Id}
				>
					{columns.map(column => {
						const value = row[column.id];
						return value === undefined ? (
							<TableCell
								key={column.id}
								align={column.align}
								padding='none'
							>
								<IconButton
									className={classes.actionButton}
									component={RouterLink}
									to={`/fitters/${row.Id}`}
								>
									<EditIcon color='primary' />
								</IconButton>
								<ConfirmDialog
									title='Usuń montera'
									content={`Wybrany monter zostanie usunięty z systemu.`}
									buttonProps={{
										icon: <DeleteIcon color='primary' />,
										className: classes.actionButton
									}}
									onConfirm={() => {
										return deleteFitter(row.Id);
									}}
								/>
							</TableCell>
						) : (
							<TableCell key={column.id} align={column.align}>
								{column.format ? column.format(value) : value}
							</TableCell>
						);
					})}
				</TableRow>
			);
		});
	};

	return (
		<>
			{!data.length ? (
				<div>nie ma</div>
			) : (
				<TableContainer className={classes.tableWrapper}>
					<Table size='small' stickyHeader aria-label='sticky table'>
						<TableHead>
							<TableRow>{createColumns(columns)}</TableRow>
						</TableHead>
						<TableBody>{createRows(data, columns)}</TableBody>
					</Table>
				</TableContainer>
			)}
		</>
	);
};

//FittersTable.whyDidYouRender = true;

export default FittersTable;
