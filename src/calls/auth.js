import client from './api-client';

export const login = async ({ login, password }) => {
	const token = `${login}:${password}:login`;

	localStorage.setItem('__auth_token__', btoa(token));

	return client('Dispatcher/CheckCredentials', 'get');
};
