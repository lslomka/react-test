import axios from 'axios';

const API_BASE_URL = 'http://10.20.1.120:4444'; //window.location.origin;

async function client(endpoint, method, { data, params, ...customConfig } = {}) {
	const token = window.localStorage.getItem('__auth_token__');
	const headers = { 'content-type': 'application/json' };

	if (token) headers.Authorization = `Basic ${token}`;

	const config = {
		method,
		timeout: 30000,
		...customConfig,
		headers: {
			...headers,
			...customConfig.headers
		}
	};

	if (data) config.data = data;
	if (params) config.params = params;

	return await axios(`${API_BASE_URL}/${endpoint}`, config);
}

export default client;
