import client from './api-client';

export const getFitters = async pageDescriptor => {
	return await client('Fitters', 'get', {
		params: { pageDescriptor: pageDescriptor }
	});
};

export const getFitter = async id => {
	return await client(`Fitter/${id}`, 'get');
};

export const addFitter = async data => {
	return await client(`Fitter`, 'post', { data });
};

export const putFitter = async (data, id) => {
	return await client(`Fitter/${id}`, 'put', { data });
};

export const deleteFitter = async id => {
	return await client(`Fitter/${id}`, 'delete');
};
