import axios from 'axios';

export const getAllCountries = async () => {
	return await axios(`https://restcountries.eu/rest/v2/all`);
};
