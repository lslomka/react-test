import { TOGGLE_DRAWER } from '../actions/application';

export const applicationReducer = (state, action) => {
	switch (action.type) {
		case TOGGLE_DRAWER:
			return {
				...state,
				drawer: action.payload.drawer
			};

		default:
			return state;
	}
};
