import { LOGIN_SUCCESS, LOGIN_ERROR, LOGOUT } from '../actions/auth';

export const authReducer = (state, action) => {
	switch (action.type) {
		case LOGIN_SUCCESS:
			localStorage.setItem('user', JSON.stringify(action.payload.user));
			return {
				...state,
				isAuthenticated: true,
				user: action.payload.user
			};

		case LOGIN_ERROR:
		case LOGOUT:
			localStorage.removeItem('user');
			localStorage.removeItem('token');
			return {
				...state,
				isAuthenticated: false,
				user: null
			};

		default:
			return state;
	}
};
