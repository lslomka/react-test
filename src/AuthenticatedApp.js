import React from 'react';
import AuthenticatedRoutes from './pages/AuthenticatedRoutes';

const AuthenticatedApp = () => {
	return (
		<>
			<AuthenticatedRoutes />
		</>
	);
};

export default AuthenticatedApp;
