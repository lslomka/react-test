import { useState, useCallback, useEffect } from 'react';
import { SERVER_ERROR_MESSAGES } from '../utils/errorMessages';
import { ERROR_TYPES } from '../utils/errorTypes';
import client from '../calls/api-client';

const useAsync = (asyncFunction, immediate = true) => {
	const [pending, setPending] = useState(false);
	const [value, setValue] = useState(null);
	const [error, setError] = useState(null);

	// function asyncFunction() {
	// 	return client('Fitters', 'get', {
	// 		params: { pageDescriptor: {} }
	// 	});
	// }

	// The execute function wraps asyncFunction and
	// handles setting state for pending, value, and error.
	// useCallback ensures the below useEffect is not called
	// on every render, but only if asyncFunction changes.
	const execute = useCallback(() => {
		setPending(true);
		setValue(null);
		setError(null);

		console.log(asyncFunction);
		return asyncFunction()
			.then(response => {
				setValue(response);
			})
			.catch(error => {
				let messages = [],
					code,
					type;

				if (error.code && error.code === 'ECONNABORTED') {
					messages.push({
						message: SERVER_ERROR_MESSAGES.timeout,
						stack: error.toJSON().stack
					});
					code = error.code;
					type = ERROR_TYPES.timeout;
				} else if (error.response) {
					const res = error.response;

					if (res.data) {
						if (res.data.SystemError) {
							messages.push({
								message: res.data.SystemError.Message,
								stack: res.data.SystemError.Raw
							});
						}
						if (res.data.DataError) {
							messages.push({
								message: res.data.DataError.Message,
								stack: res.data.DataError.Raw
							});
						}
					} else {
						messages.push({
							message: res.statusText,
							stack: error.toJSON().stack
						});
					}

					code = res.status;
					type = ERROR_TYPES.api;
				} else if (error.request) {
					const res = error.toJSON();

					messages.push({
						message: res.message,
						stack: res.stack
					});

					code = ERROR_TYPES.server;
				} else {
					console.log(error.request);
					console.log(error.toJSON());
					// setType(ERROR_TYPES.client);
				}
				setError({ messages, code, type });
			})
			.finally(() => setPending(false));
	}, [asyncFunction]);

	// Call execute if we want to fire it right away.
	// Otherwise execute can be called later, such as
	// in an onClick handler.
	useEffect(() => {
		if (immediate) {
			execute();
		}
	}, [execute, immediate]);

	return { execute, pending, value, error, setImmediate };
};

export default useAsync;
