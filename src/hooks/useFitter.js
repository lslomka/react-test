import { useContext, useCallback, useEffect, useState } from 'react';
import { FitterContext } from '../contexts/fitter';
import { getFitters } from '../calls/fitters';
import errorHandler from '../utils/errorHandler';

const useFitter = (execute = true) => {
	const context = useContext(FitterContext);
	const [pending, setPending] = useState(false);
	const [value, setValue] = useState(null);
	const [error, setError] = useState(null);

	const fetchData = useCallback(async () => {
		setPending(true);

		try {
			const resp = await getFitters(context.pageDescriptor);

			context.onDataFetch(resp);
		} catch (error) {
			setError(errorHandler(error));
		}

		setPending(false);
	}, [context.pageDescriptor]);

	useEffect(() => {
		if (execute) fetchData();
		return () => {
			context.clear();
		};
	}, [fetchData, execute]);

	return { ...context, pending, value, error };
};

export default useFitter;
