import { useState, useEffect, useCallback } from 'react';
import { SERVER_ERROR_MESSAGES } from '../utils/errorMessages';
import { ERROR_TYPES } from '../utils/errorTypes';

const useApiError = error => {
	const [messages, setMessages] = useState([]);
	const [status, setStatus] = useState(null);
	const [type, setType] = useState(null);

	console.log(error.response);
	console.log(error.request);

	const execute = useCallback(() => {
		if (error.code && error.code === 'ECONNABORTED') {
			setMessages({
				message: SERVER_ERROR_MESSAGES.timeout,
				stack: error.toJSON().stack
			});
			setStatus(error.code);
			setType(ERROR_TYPES.timeout);
		} else if (error.response) {
			const res = error.response;

			if (res.data) {
				if (res.data.SystemError) {
					setMessages({
						message: res.data.SystemError.Message,
						stack: res.data.SystemError.Raw
					});
				}
				if (res.data.DataError) {
					setMessages({
						message: res.data.DataError.Message,
						stack: res.data.DataError.Raw
					});
				}
			} else {
				setMessages({
					message: res.statusText,
					stack: error.toJSON().stack
				});
			}

			setStatus(res.status);
			setType(ERROR_TYPES.api);
		} else if (error.request) {
			console.log(error.response);
			// setType(ERROR_TYPES.server);
		} else {
			console.log(error.response);
			// setType(ERROR_TYPES.client);
		}
	});

	useEffect(() => {
		execute();
	}, [execute]);

	return { messages, type, status };
};

export default useApiError;
